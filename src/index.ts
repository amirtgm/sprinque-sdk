interface ISprinqueSDK {
  publicKey: string;
}
export class SprinqueSDK {
  publicKey: string;
  serverUrl: string;
  constructor({publicKey}: ISprinqueSDK){
    this.publicKey = publicKey;
    // this.serverUrl = 'https://api.sprinque.com';
    this.serverUrl = 'localhost:2132';
  }
  fetchInstance(url: string) {
    const headers = new Headers();
    headers.set('publicKey', this.publicKey);
    return fetch(this.serverUrl + '/' + url , {headers});
  } 
  async getBanks() {
    return this.fetchInstance('banks')
  }
  async getTransactionData(transactionID : string){
    return this.fetchInstance('transaction/' + transactionID)
  }
  async getCompany() {
    return this.fetchInstance('company')
  }
  
}

