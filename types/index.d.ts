interface ISprinqueSDK {
    publicKey: string;
}
export declare class SprinqueSDK {
    publicKey: string;
    serverUrl: string;
    constructor({ publicKey }: ISprinqueSDK);
    fetchInstance(url: string): Promise<Response>;
    getBanks(): Promise<Response>;
    getTransactionData(transactionID: string): Promise<Response>;
    getCompany(): Promise<Response>;
}
export {};
//# sourceMappingURL=index.d.ts.map